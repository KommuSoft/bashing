#!/bin/bash

echo -n "Group,"
head "$1" -n 1

for i in `seq 1 $#`
do
	fl=$(tail "${!i}" -n +2)
	nl=$(echo "$fl" | wc -l)
	paste -d',' <(echo "$i" | perl -ne "for\$i(1..$nl){print}") <(echo "$fl")
done
