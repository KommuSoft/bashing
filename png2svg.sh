#!/bin/bash
#Kudos to @ykarikos

if [ "$1" == "" ]; then
  echo Usage: $0 pngfile
  exit 0
fi

file=`basename $1 .png`
pnmf="/tmp/png2svg-$file.pnm"

if [ ! -e $file.png ]; then
  echo $file.png does not exist
  exit 1
fi

convert $file.png "$pnmf"
potrace -O 10.0 -a 0 -s -o $file.svg "$pnmf"
rm "$pnmf"
