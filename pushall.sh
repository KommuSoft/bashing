#!/bin/bash
rmts=$(git remote)

for r in $rmts
do
	git push --all "$r" 2> /dev/null
	git push --tags "$r" 2> /dev/null
	git submodule foreach "git push --all \"$r\"" 2> /dev/null
	git submodule foreach "git push --tags \"$r\"" 2> /dev/null
done
