#!/bin/bash
res=$(netstat -an)
for tp in {LISTENING,TIME-WAIT,ESTABLISHED}
do
    n=$(echo "$res" | grep "$tp" | wc -l)
    echo "$tp: $n"
done
