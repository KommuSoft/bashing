#!/bin/bash
lines=$(cat)
cols=$(echo "$lines" | tail -n 1 | awk -F"\t" '{print NF; exit}')
mkdir -p "/tmp/cuts"
echo "$lines" | cut -f "1" | sort -g > "/tmp/cuts/temp"
for f in $(seq 2 $cols)
do
	echo "$lines" | cut -f "$f" | sort -g | paste "/tmp/cuts/temp" - > "/tmp/cuts/temp2"
	mv -f "/tmp/cuts/temp2" "/tmp/cuts/temp"
done
cat "/tmp/cuts/temp"
rm -rf "/tmp/cuts"
