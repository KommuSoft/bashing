Bashing
=======

A set of `bash` scripts that are used for convenience.

Installation
------------

The `bash` scripts require a lot of packages. In order to install everything at once, run:

```
sudo bash install.sh
```

Commands
--------

 * `block.sh`: A shell script that is used to study 50 minutes (with music) and then taking pause for 10 minutes (and `git` commits).
 * `citewebsite.sh`: fetch data from the given website (first argument) and generate a `bibtex` entry that is printed on the `stdout`.
 * `commitallrepo.sh`: A shell script that searches for `git` repositories and commits the current state of the project.
 * `createvhdlproject.sh`: A shell script to initialize a vhdl project with file structure and `Makefile`.
 * `fetchall.sh`: Fetches the data from all remote repositories of the current `git` repository.
 * `generatedocumentation.sh`: Generates the documentation of a `mono` project on the `gh-pages` of a `git` repository.
 * `makemono.sh`: Run the build script of a `mono` project.
 * `pushall.sh`: Push all the commits (of all branches) of the repository to all registered remotes.
 * `scraprtmp.sh` : scrap a website (mainly `deredactie.be`) in search for media files that are downloaded using rtmp.
 * `tospeak.sh`: Given a `.pdf` file as argument, the script extracts the text out of the file, removes noise, splits it into chapters, and generates per chapter an `.mp3` file with the spoken version of the file using `lame`.
 * `unswap.sh`: Move all the data in the `swap` space back to the active memory. This shell script requires `root` access.
 * `updatesystem.sh`: Updates the system software. This shell script requires `root` access.
 * `weeklydoc.sh`: Generate `mono` project documentation on the `gh-pages` branch of a project on a weekly basis.
