aligntimeline :: [(Int,Int)] -> [Int]
aligntimeline = aligntimeline' []

aligntimeline' :: [Int] -> [(Int,Int)] -> [Int]
aligntimeline' _ [] = []
aligntimeline' ln (x:xs) = (ix:aligntimeline' lna xs)
    where (lna,ix) = schedule ln x 0

schedule :: [Int] -> (Int,Int) -> Int -> ([Int],Int)
schedule [] (_,e) ix = ([e],ix)
schedule (te:tes) (b,e) ix | te <= b = ((e:tes),ix)
                           | otherwise = (te:lna,rix)
                           where (lna,rix) = schedule tes (b,e) (ix+1)

group2 :: [a] -> [(a,a)]
group2 [] = []
group2 [_] = []
group2 (aa:ab:as) = ((aa,ab):group2 as)

main = do
	interact (unwords . map (show) . aligntimeline . group2 . map (read :: String -> Int) . words)
