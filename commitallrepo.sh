#!/bin/bash
notify-send "Start committing and pushing repositories"
sleep 120
killall firefox
cd ~
find -name ".git" | while read r
do
	if [ -d "$r" ]
	then
		cd "$r/.."
		
		brnc=$(git rev-parse --abbrev-ref HEAD)
		rmts=$(git remote)
		timeout 600 git fetch --all 2> /dev/null || true
		timeout 100 git config credential.helper store || true
		timeout 100 git config user.signingkey 8B09E77C || true
		timeout 100 git config commit.gpgsign || true
		timeout 100 git commit -S -q -a -m "temp commit (by cron)" || true
		timeout 100 git commit -q -a -m "temp commit (by cron)" || true
		for s in $rmts
		do
			timeout 100 git push --all "$s" 2> /dev/null || true
			timeout 100 git push --tags "$s" 2> /dev/null || true
		done
		git gc --aggressive
		git checkout "$brnc"
	fi
	cd ~
done
notify-send "[done] (committing and pushing repositories)"
