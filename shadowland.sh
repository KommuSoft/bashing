#!/bin/bash

# Turns all the files in the active directory to zero byte files.
# WARNING: This script is dangerous!

# The script is useful if one wants to generate "representatives" for real file
# content: for example, one wishes to copy files on a usb drive, but some files
# are already copied. Instead of doing bookkeeping oneself, or copying the same
# files over again, one can create "shadow files" so that duplicate copies can
# be skipped.

for f in **
do
	echo -n "" > "$f"
done
