#!/bin/bash
#fetch page
page=$(wget --keep-session-cookies --load-cookies=/dev/stdin -q -S -O - "$1" 2> /dev/null | tidy -asxhtml -numeric 2> /dev/null)
#search forms
nforms=$(echo "$page" | xmllint --html --xpath 'count(//form)' - 2>/dev/null)
echo "Detected $nforms form(s)..."
#form disambiguation
if [ $nforms -gt 0 ]; then
	if [ $nforms -gt 1 ]; then
		forms=$(echo "$page" | xmllint --html --xpath '//form/@action' - 2>/dev/null | grep -P 'action=\"[^"]*\"' -o | cut -d '"' -f 2)
		options=$(paste <(seq 1 $nforms) <(echo "$forms") --delimiters ' ')
		echo "$options"
		rslt=$(dialog --stdout --clear --title "Form disambiguation" --menu "Please select action of the form to process?" 11 100 5 $options)
		stts=$?
		if [ "$stts" -eq 0 ]
		then
			ifrm=$(echo "$page" | xmllint --html --xpath "descendant::form[$rslt]" - 2>/dev/null)
		else
			echo "Cancelled..."
			exit
		fi
	else
		ifrm=$(echo "$page" | xmllint --html --xpath '//form[1]' - 2>/dev/null)
	fi
else
	echo "No forms detected, I will exit now"
	exit
fi
#select items
methd=$(echo "$ifrm" | xmllint --html --xpath '//form/@method' - 2>/dev/null | grep -Po '".*?"' | tr -d \")
actio=$(echo "$ifrm" | xmllint --html --xpath '//form/@action' - 2>/dev/null | grep -Po '".*?"' | tr -d \")
names=$(echo "$ifrm" | xmllint --html --xpath '//input/@name|//select/@name' - 2>/dev/null | grep -Po '".*?"' | tr -d \")
echo 'while [[ $# > 1 ]]; do'
echo '    key="$1"; shift'
echo '    case $key in';
for f in $names
do
	echo "        --$f)"
	echo "            $f=\$1"
	echo "            ;;"
done
echo "        *)"
echo '            echo "Did not recognize option $key"';
echo '            ;;';
echo '    esac'
echo '    shift'
echo 'done'
for f in $names
do
	d=$(echo "$ifrm" | xmllint --html --xpath "normalize-space(//input[@name=\"$f\"]/../..|//select[@name=\"$f\"]/../..)" - 2>/dev/null | head -n 1)
	echo "if [[ -n -v $f ]]"
	echo 'then'
	echo "    read -e -p \"$d:\" $f" #TODO: search for description
	echo 'fi'
done
echo "curl -X $methd $action"
