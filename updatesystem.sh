#!/bin/bash
while [[ "$1" == "ppa" ]]
do
    shift
    rep="$1"
    if [[ "$1" != "ppa:*" ]]
    then
    	rep="ppa:$1"
    fi
    sudo add-apt-repository "$rep"
    shift
done
sudo apt-get update > /dev/null
sudo apt-get upgrade
sudo apt-get autoremove

if [ $# -gt 0 ]
then
    sudo apt-get $@
fi
