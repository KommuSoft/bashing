#!/bin/bash
drs=$(dirname "$2")
fbs=$(basename "$2" ".dll")
fbs=$(basename "$fbs" ".exe")
brn=$(git rev-parse --abbrev-ref HEAD)
git add .
git stash
git checkout master
mdtool build "$1" > /dev/null
monodocer -pretty -importslashdoc:"$drs/$fbs.xml" -assembly:"$2" -path:"/tmp/xmldoc" >/dev/null 2>/dev/null
until mdoc export-html -o /tmp/htmldoc /tmp/xmldoc >/dev/null 2>/dev/null
do
	sleep 1
done
git checkout "gh-pages" || true
git rm -rf . || true
rm -rf * || true
cp -R /tmp/htmldoc/* . || true
git add . || true
git commit -am "New version of documentation" || true
git checkout "$brn" || true
git submodule update --init --recursive || true
bash ~/pushall.sh || true

rm -rf /tmp/xmldoc
rm -rf /tmp/htmldoc
