#!/bin/bash
rhythmbox-client --hide
rhythmbox-client --set-volume 0.3
while true
do
	speaker-test -t sine -b 1000 -p 1000 -f 1000 -l 2 2>/dev/null
	rhythmbox-client --play
	echo -n "Next stop: "
        date --date "50 minutes"
	read -t 3000
	rhythmbox-client --pause
	speaker-test -t sine -b 1000 -p 1000 -f 1000 -l 2 2>/dev/null
	read -t 600
done
