.\" Manpage for updatesystem.sh.
.\" Contact Willem.VanOnsem@cs.kuleuven.be to correct errors or typos.
.TH man 8 "2 Feb 2015" "0.2" "updatesystem.sh man page"
.SH NAME
updatesystem.sh \- convenient software updating.
.SH SYNOPSIS
updatesystem.sh [OPTIONS] [apt-get OPTIONS]
.SH DESCRIPTION
A tool to update the packages installed on the system that also provides 
.SH OPTIONS
.TP
.B ppa "ppa-repository"
Add the given repository to the list of registered repositories before updating, upgrading and optionally running apt-get commands.
.TP
The remaining options passed to the script are passed to a separate "sudo apt-get" call (if any given).
One can thus for instance use "sudo updatesystem.sh install package" to install an additional package after updating.
.SH SEE ALSO
.SH BUGS
No known bugs.
.SH AUTHOR
Willem Van Onsem (Willem.VanOnsem@cs.kuleuven.be)
