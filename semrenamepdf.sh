#!/bin/bash
mkdir -p titled
for f in *.pdf
do
	#A title must have at least 10 alpha characters
	tit=$(exiftool "$f" | grep "^Title" | head -n 1 | cut -d ':' -f2- | cut -d ' ' -f2- | grep -P '([a-zA-Z].){10,}')
	if [[ -n $tit ]]
	then
		#A title does not end with an extension
		if [[ $(echo "$tit" | grep -v -P '\..{1,5}$') ]]
		then
			rtt=$(echo "$tit" | sed 's/[ /\:*?"<>|]/_/g')
			echo "rename \"$f\" to \"$rtt.pdf\""
			mv -n "$f" "titled/$rtt.pdf"
		fi
	fi
done
